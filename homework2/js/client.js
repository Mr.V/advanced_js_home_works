/**
 * Add or del burger values
 * */
var addBigBurg = document.querySelector('.add-big-burger'),
    delBigBurg = document.querySelector('.cancel-big-burger'),
    addSmlBurg = document.querySelector('.add-small-burger'),
    delSmlBurg = document.querySelector('.cancel-small-burger');

/**
 * Returning burger values
 * */
var burgerSize     = document.querySelector('.size'),
    burgerAdding   = document.querySelector('.adding'),
    burgerStuffing = document.querySelector('.stuffing'),
    burgerPrice    = document.querySelector('.add-big-burger'),
    burgerCalories = document.querySelector('.add-big-burger');

/**
 * Show burger's size
 * */
addBigBurg.addEventListener('onclick', function () {
    burgerSize.innerHTML = Hamburger.SIZE_LARGE;
});

addSmlBurg.addEventListener('onclick', function () {
    burgerSize.innerHTML = Hamburger.SIZE_SMALL;
});

delBigBurg.addEventListener('onclick', function () {
    burgerSize.innerHTML = ' ';
});

delSmlBurg.addEventListener('onclick', function () {
    burgerSize.innerHTML = ' ';
});

/**
 * Show burger's adding
 * */




