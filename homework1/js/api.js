/**
 * Класс, объекты которого описывают параметры гамбургера. 
 * 
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    this._size = size;
    this._stugging = stuffing;
    this._topping = [];
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = { size: 'Small', price: 50, calories: 20 };
Hamburger.SIZE_LARGE = { size: 'Large', price: 100, calories: 40 };
Hamburger.STUFFING_CHEESE = { stuffing: 'Cheese', price: 10, calories: 20 };
Hamburger.STUFFING_SALAD = { stuffing: 'Salad', price: 20, calories: 5 };
Hamburger.STUFFING_POTATO = { stuffing: 'Potato', price: 15, calories: 10 };
Hamburger.TOPPING_MAYO = { topping: 'Mayo', price: 20, calories: 5 };
Hamburger.TOPPING_SPICE = { topping: 'Spice', price: 15, calories: 0 };

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 * 
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function(topping) {
    if (!this._topping.includes(topping)) {
        this._topping.push(topping);
    }
    // else {
    //     HamburgerException();
    // }
};

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function(topping) {
    for (var i = 0; i < this.toppings.length; i++) {
        if (this.toppings[i] === topping) {
            this.toppings.splice(i, 1);
        }
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function() {
    return this._topping;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function() {
    return this._size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function() {
    return this._stugging;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function() {
    var toppingsPrice = this._topping.reduce((total, topping) => total + topping.price, 0);
    return this._stugging.price + this._size.price + toppingsPrice;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function() {
    var toppingsCalories = this._topping.reduce((total, topping) => total + topping.calories, 0);
    return this._stugging.calories + this._size.calories + toppingsCalories;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
//function HamburgerException() {}