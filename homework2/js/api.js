/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
class Hamburger {
    constructor(size, stuffing) {
        this._size = size;
        this._stuffing = stuffing;
        this._toppings = [];
    }

    addTopping(topping) {
        if (!this._toppings.includes(topping)) {
            return this._toppings.push(topping);
        };
    }

    removeTopping(topping) {
        return this._toppings = this._toppings.filter(x => x !== topping);
    }

    get getToppings() {
        return this._toppings;
    }

    get getSize() {
        return this._size;
    }

    get getStuffing() {
        return this._stuffing
    }

    get calculatePrice() {
        const priceArr = this._toppings.map(x => Hamburger.TOPPINGS[x].price);
        priceArr.push(Hamburger.SIZES[this._size].price, Hamburger.STUFFINGS[this._stuffing].price);
        let price = priceArr.reduce((acc, prices) => acc + prices, 0);
        return price;
    }

    get calculateCalories() {
        const caloriesArr = this._toppings.map(x => Hamburger.TOPPINGS[x].calories);
        caloriesArr.push(Hamburger.SIZES[this._size].calories, Hamburger.STUFFINGS[this._stuffing].calories);
        let calories = caloriesArr.reduce((acc, itemcalories) => acc + itemcalories, 0);
        return calories;
    }

    // function HamburgerException () {
    //
    // }
}

Hamburger.SIZE_SMALL = 'Small Burger';
Hamburger.SIZE_LARGE = 'Big Burger';
Hamburger.SIZES = {
    [Hamburger.SIZE_SMALL]: {
        price: 50,
        calories: 20
    },

    [Hamburger.SIZE_LARGE]: {
        price: 100,
        calories: 40
    }
};

Hamburger.STUFFING_CHEESE = 'Stuffing Cheese';
Hamburger.STUFFING_SALAD = 'Stuffing Salad';
Hamburger.STUFFING_POTATO = 'Stuffing Potato';
Hamburger.STUFFINGS = {
    [Hamburger.STUFFING_CHEESE]: {
        price: 10,
        calories: 20
    },

    [Hamburger.STUFFING_SALAD]: {
        price: 20,
        calories: 5
    },

    [Hamburger.STUFFING_POTATO]: {
        price: 15,
        calories: 10
    }
};


Hamburger.TOPPING_MAYO = 'Topping Mayo';
Hamburger.TOPPING_SPICE = 'Topping Spice';
Hamburger.TOPPINGS = {
    [Hamburger.TOPPING_MAYO]: {
        price: 20,
        calories: 5
    },

    [Hamburger.TOPPING_SPICE]: {
        price: 15,
        calories: 0
    }
};




